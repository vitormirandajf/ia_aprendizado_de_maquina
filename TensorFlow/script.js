const xs = tf.tensor([1,4,5,6,8,10]);
const ys = xs.mul(0.5).add(0.7);

const model = tf.sequential();
model.add(tf.layers.dense({units:1, inputShape:[1]}));

model.compile({loss: 'meanSquaredError', optimizer:'sgd'});

model.fit(xs, ys, {epochs:500}).then(() => {myFunction()});


function myFunction(){
    const xMax = 10;
    const xArr = [];
    const yArr = [];

    for(let x = 0; x <= xMax; x++){
        let result = model.predict(tf.tensor([Number(x)]));
        result.data().then(y =>{
            xArr.push(x);
            yArr.push(Number(y));

            if(x == xMax) {plot(xArr, yArr)};
        });
    }
}
function plot(xArr, yArr){
    const data = [{x:xArr,y:yArr, mode:'markers', type:'scatter'}];

    const layout = {
        xaxis: { range:[0,10]},
        yaxis: { range:[0,20]},
    };

    Plotly.newPlot("tensor", data, layout);
}

